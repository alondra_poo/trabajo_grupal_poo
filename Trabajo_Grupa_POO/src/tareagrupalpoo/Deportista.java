package tareagrupalpoo;

import java.util.Calendar;

public class Deportista extends Persona{
    private int genero;
    private int deporte;
private Calendar cal= Calendar.getInstance();
    public Deportista(String nombre, String apellido, String id, int anioNac, String celular, int genero, int deporte) {
        super(nombre, apellido, id, anioNac, celular);
        this.genero=genero;
        this.deporte=deporte;
    }
    public String imprimir() {
      return "--Deportista-- \nNombre: "+this.getNombre()+"\nApellido: "+this.getApellido()+
              "\nIdentidad: "+this.getId()+"\nAnio Nacimiento: "+this.getAnioNac()+
              "\nCelular: "+this.getCelular()+"\nEdad: "+calEdad()+"\nGenero: "+genero(this.getGenero())+
              "\nDeporte que practica: "+this.deporteJugador(this.getDeporte())+"\n";
    }
    public int calEdad(){
        return cal.get(Calendar.YEAR)-this.getAnioNac();
    }
    public String genero(int g) {
        String gen="";
        switch(g){
            case 1: 
                gen="Masculino";
                break;
            case 2:
                gen="Feminino";
                break;
            default:
        }
    return gen;
    }
  public String deporteJugador(int n){
      String dep="";
      switch(n){
          case 1:
              dep="Futbol";
              break;
          case 2:
              dep="Natacion";
              break;
          case 3:
              dep="Basketball";
              break;
          case 4:
              dep="Beisball";
              break;
          case 5:
              dep="Futbol Americano";
              break;
          default:
              dep="Otro";
      }
      return dep;
  }
    public int getGenero() {
        return genero;
    }

    public void setGenero(int genero) {
        this.genero = genero;
    }

    public int getDeporte() {
        return deporte;
    }

    public void setDeporte(int deporte) {
        this.deporte = deporte;
    }
    
}
