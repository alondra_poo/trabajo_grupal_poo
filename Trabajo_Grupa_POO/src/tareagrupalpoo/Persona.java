
package tareagrupalpoo;

public abstract class Persona {
    private String nombre;
    private String apellido;
    private String id;
    private int anioNac;
    private String celular;
    
    /*AREA DE METODOS ABSTRACTOS*/
    public abstract String imprimir();
    public abstract int calEdad();
    public abstract String genero(int g);
    
    /*DECLARAMOS NUESTRO CONSTRUCTOR*/

    public Persona(String nombre, String apellido, String id, int anioNac, String celular) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.id = id;
        this.anioNac = anioNac;
        this.celular = celular;
    }
    /*CREAMOS TODOS NUESTROS (SETTERS y GETTERS)*/

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAnioNac() {
        return anioNac;
    }

    public void setAnioNac(int anioNac) {
        this.anioNac = anioNac;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }
    
}
