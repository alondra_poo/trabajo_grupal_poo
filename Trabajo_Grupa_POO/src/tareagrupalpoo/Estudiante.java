package tareagrupalpoo;

import java.util.Calendar;

public class Estudiante extends Persona{
    private int genero;
private Calendar cal= Calendar.getInstance();
    public Estudiante(String nombre, String apellido, String id, int anioNac, String celular, int genero) {
        super(nombre, apellido, id, anioNac, celular);
        this.genero=genero;
    }
    public String imprimir() {
      return "--Estudiante-- \nNombre: "+this.getNombre()+"\nApellido: "+this.getApellido()+
              "\nIdentidad: "+this.getId()+"\nAnio Nacimiento: "+this.getAnioNac()+
              "\nCelular: "+this.getCelular()+"\nEdad: "+calEdad()+"\nGenero: "+genero(this.getGenero())+"\n\n";
    }
    public int calEdad(){
        return cal.get(Calendar.YEAR)-this.getAnioNac();
    }
    public String genero(int g) {
        String gen="";
        switch(g){
            case 1: 
                gen="Masculino";
                break;
            case 2:
                gen="Feminino";
                break;
            default:
        }
    return gen;
    }

    public int getGenero() {
        return genero;
    }

    public void setGenero(int genero) {
        this.genero = genero;
    }   
}
